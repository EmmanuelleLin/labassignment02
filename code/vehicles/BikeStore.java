//2239468 Emmanuelle Lin
package vehicles;

public class BikeStore {
    public static void main (String[] args){
        
        Bicycle[] bicycles = new Bicycle[4];

        bicycles[0] = new Bicycle("Speedo", 20, 20);
        bicycles[1] = new Bicycle("Vroom vroom", 10, 15);
        bicycles[2] = new Bicycle("Fast&Furious", 18, 45);
        bicycles[3] = new Bicycle("Kachow", 36, 50);

        for(Bicycle x:bicycles){
            System.out.println(x);
        }
    }
}
