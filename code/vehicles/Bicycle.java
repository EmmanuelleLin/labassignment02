//Emmanuelle Lin 2239468
package vehicles;
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, int maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String getManu(){
        return this.manufacturer;
    }

    public int getGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public String toString(){
        return "Manufacturer:" + this.manufacturer + ", Number of gears:" + this.numberGears + ", MaxSpeed:" + this.maxSpeed;
    }
}